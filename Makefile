all: build_base build_stack

build_base:
	docker build -t thousand:base -f dockerfiles/Dockerfile-webserver-base .

build_stack:
	docker-compose build
	docker-compose up -d
