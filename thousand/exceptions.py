from flask import jsonify
from werkzeug.exceptions import HTTPException, default_exceptions


class JSONExceptionHandler(object):
    def __init__(self, app=None):
        if app:
            self.init_app(app)

    def std_handler(self, error):
        response = jsonify(message={'error': error.message, 'payload': error.payload})
        response.status_code = error.code if isinstance(error, HTTPException) else 500
        return response

    def init_app(self, app):
        self.app = app
        self.register(HTTPException)
        for code, v in default_exceptions.items():
            self.register(code)

    def register(self, exception_or_code, handler=None):
        self.app.errorhandler(exception_or_code)(handler or self.std_handler)


class BasicException(HTTPException):
    code = 400
    message = 'Basic Exception. Should never be called'

    def __init__(self, payload: dict = dict({})):
        super(HTTPException, self).__init__()
        self.payload = payload


class PasswordGenerationException(BasicException):
    code = 400
    message = 'Could not generate room code'


class RequestParameterException(BasicException):
    code = 400
    message = 'Request data have wrong values'


class ResourceNotFound(BasicException):
    code = 404
    message = 'Requested resource does not exist'


class RoomJoinException(BasicException):
    code = 400
    message = 'Can not join to this room'
