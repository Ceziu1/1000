import json
from typing import Optional

from thousand.json import ThousandJSONEncoder
from thousand.models.card import Card, CardSchema, Color, PlayerCard
from thousand.models.room import PrivateRoom, PrivateRoomSchema, PublicRoom, PublicRoomSchema
from thousand.redis import Cache, RedisKeys, RedisLock, RedisLockKeys


class RedisConverter:

    ### PRIVATE ROOM ###
    @staticmethod
    def get_taken_codes() -> list[str]:
        with RedisLock(RedisLockKeys.TAKEN_ROOM_CODES, Cache()) as lock:
            taken_codes: list[str] = json.loads(lock.client.get(RedisKeys.TAKEN_ROOM_CODES, '[]'))

        return taken_codes

    @staticmethod
    def add_taken_code(code: str) -> None:
        taken_codes: list[str] = RedisConverter.get_taken_codes()
        taken_codes.append(code)

        with RedisLock(RedisLockKeys.TAKEN_ROOM_CODES, Cache()) as lock:
            lock.client.set(RedisKeys.TAKEN_ROOM_CODES, json.dumps(taken_codes))

    @staticmethod
    def remove_taken_code(code: str) -> None:
        taken_codes: list[str] = RedisConverter.get_taken_codes()
        taken_codes = list(filter(lambda x: x != code, taken_codes))

        with RedisLock(RedisLockKeys.TAKEN_ROOM_CODES, Cache()) as lock:
            lock.client.set(RedisKeys.TAKEN_ROOM_CODES, json.dumps(taken_codes))

    @staticmethod
    def get_private_rooms() -> list[PrivateRoom]:
        schema: PrivateRoomSchema = PrivateRoomSchema()

        with RedisLock(RedisLockKeys.PRIVATE_ROOM, Cache()) as lock:
            available_rooms: list[dict] = json.loads(lock.client.get(RedisKeys.PRIVATE_ROOMS_LIST, '[]'))
            available_private_rooms: list[PrivateRoom] = schema.load(available_rooms, many=True)

        return available_private_rooms

    @staticmethod
    def add_private_room(room: PrivateRoom) -> None:
        schema: PrivateRoomSchema = PrivateRoomSchema()
        available_rooms: list[PrivateRoom] = RedisConverter.get_private_rooms()
        available_rooms.append(room)

        with RedisLock(RedisLockKeys.PRIVATE_ROOM, Cache()) as lock:
            lock.client.set(RedisKeys.PRIVATE_ROOMS_LIST, schema.dumps(available_rooms, many=True))

    @staticmethod
    def remove_private_room(room_id: str) -> None:
        schema: PrivateRoomSchema = PrivateRoomSchema()
        available_rooms: list[PrivateRoom] = RedisConverter.get_private_rooms()
        room_code: str = next(filter(lambda x: x.id == room_id, available_rooms)).password
        RedisConverter.remove_taken_code(room_code)

        available_rooms = list(filter(lambda x: x.id != room_id, available_rooms))
        with RedisLock(RedisLockKeys.PRIVATE_ROOM, Cache()) as lock:
            lock.client.set(RedisKeys.PRIVATE_ROOMS_LIST, schema.dumps(available_rooms, many=True))

    ### CONNECTED PLAYERS ###
    @staticmethod
    def get_connected_players(room_id: str) -> list[str]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            players: list[str] = json.loads(lock.client.get(RedisKeys.CONNECTED_PLAYERS.format(room_id), '[]'))

        return players

    @staticmethod
    def add_connected_player(room_id: str, player_id: str) -> None:
        connected_players: list[str] = RedisConverter.get_connected_players(room_id)
        connected_players.append(player_id)

        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.CONNECTED_PLAYERS.format(room_id), json.dumps(connected_players))

    ### ROOM HOST ###
    @staticmethod
    def add_room_host(room_id: str, player_id: str) -> None:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.ROOM_HOST.format(room_id), player_id)

    @staticmethod
    def get_room_host(room_id: str) -> str:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            player_id: str = lock.client.get(RedisKeys.ROOM_HOST.format(room_id))

        return player_id

    ### PUBLIC ROOM ###
    @staticmethod
    def get_public_rooms() -> list[PublicRoom]:
        schema: PublicRoomSchema = PublicRoomSchema()

        with RedisLock(RedisLockKeys.PUBLIC_ROOM, Cache()) as lock:
            available_rooms: list[dict] = json.loads(lock.client.get(RedisKeys.PUBLIC_ROOMS_LIST, '[]'))
            available_public_rooms: list[PublicRoom] = schema.load(available_rooms, many=True)

        return available_public_rooms

    @staticmethod
    def add_public_room(room: PublicRoom) -> None:
        schema: PublicRoomSchema = PublicRoomSchema()
        available_rooms: list[PublicRoom] = RedisConverter.get_public_rooms()
        available_rooms.append(room)

        with RedisLock(RedisLockKeys.PUBLIC_ROOM, Cache()) as lock:
            lock.client.set(RedisKeys.PUBLIC_ROOMS_LIST, schema.dumps(available_rooms, many=True))

    @staticmethod
    def remove_public_room(room_id: str) -> None:
        schema: PublicRoomSchema = PublicRoomSchema()
        available_rooms: list[PublicRoom] = RedisConverter.get_public_rooms()
        available_rooms = list(filter(lambda x: x.id != room_id, available_rooms))

        with RedisLock(RedisLockKeys.PUBLIC_ROOM, Cache()) as lock:
            lock.client.set(RedisKeys.PUBLIC_ROOMS_LIST, schema.dumps(available_rooms, many=True))

    ### GAME RUNNING ###
    @staticmethod
    def is_game_pending(room_id: str) -> bool:
        game_not_pending: str = '0'

        with RedisLock(RedisLockKeys.ROOM, Cache()) as lock:
            is_game_running: int = lock.client.get(RedisKeys.GAME_PENDING.format(room_id), game_not_pending)

        if is_game_running == game_not_pending:
            return True
        return False

    @staticmethod
    def set_game_state(room_id: str, state: int) -> None:
        with RedisLock(RedisLockKeys.ROOM, Cache()) as lock:
            lock.client.set(RedisKeys.GAME_PENDING.format(room_id), state)

    ### DECK ###
    @staticmethod
    def get_deck(room_id: str) -> list[Card]:
        # schema: CardSchema = CardSchema()

        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            db_deck: list[dict] = json.loads(lock.client.get(RedisKeys.STACK, '[]'))
            deck: list[Card] = [Card(**c) for c in db_deck]
            # deck: list[Card] = schema.load(db_deck, many=True)

        return deck

    @staticmethod
    def set_deck(room_id: str, deck: list[Card]) -> None:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.STACK.format(room_id), json.dumps(deck, cls=ThousandJSONEncoder))

    ### CARDS ON HAND ###
    @staticmethod
    def get_cards_on_hand(room_id: str) -> dict[str, list[Card]]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            db_cards: dict[str, list[dict]] = json.loads(lock.client.get(RedisKeys.CARDS_ON_HAND.format(room_id), '[]'))

        cards_on_hand: dict[str, list[Card]] = {k: [Card(**c) for c in v] for k, v in db_cards.items()}
        return cards_on_hand

    @staticmethod
    def set_cards_on_hand(room_id: str, cards: dict[str, list[Card]]) -> None:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.CARDS_ON_HAND.format(room_id), json.dumps(cards, cls=ThousandJSONEncoder))

    ### CARDS ON TABLE ###
    @staticmethod
    def get_cards_on_table(room_id: str) -> list[PlayerCard]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            db_cards: list[tuple[str,
                                 dict]] = json.loads(lock.client.get(RedisKeys.CARDS_ON_TABLE.format(room_id), '[]'))

        cards_on_table: list[PlayerCard] = [PlayerCard(k, Card(**c)) for k, c in db_cards]
        return cards_on_table

    @staticmethod
    def add_card_on_table(room_id: str, player_id: str, card: Card):
        cards_on_table: list[PlayerCard] = RedisConverter.get_cards_on_table(room_id)

        cards_on_table.append(PlayerCard(player_id, card))
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(
                RedisKeys.CARDS_ON_TABLE.format(room_id), json.dumps(cards_on_table, cls=ThousandJSONEncoder)
            )

    @staticmethod
    def clear_cards_from_table(room_id: str):
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.delete(RedisKeys.CARDS_ON_TABLE.format(room_id))

    ### COMBINATION ###
    @staticmethod
    def get_combination(room_id: str) -> Optional[Color]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            color: Optional[str] = lock.client.get(RedisKeys.COMBINATION.format(room_id))

        return Color[color] if color else None

    @staticmethod
    def set_combination(room_id: str, color: Color) -> None:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.COMBINATION.format(room_id), color.name)

    @staticmethod
    def get_deal_combination_points(room_id: str) -> dict[str, int]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            combinations: dict[str,
                               int] = json.loads(lock.client.get(RedisKeys.COMBINATION_POINTS.format(room_id), '{}'))

        return combinations

    @staticmethod
    def add_deal_combination_points(room_id: str, player_id: str, points: int) -> None:
        combinations: dict[str, int] = RedisConverter.get_deal_combination_points(room_id)

        if combinations.get(player_id):
            combinations[player_id] += points
        else:
            combinations[player_id] = points

        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.COMBINATION_POINTS.format(room_id), json.dumps(combinations))

    @staticmethod
    def remove_deal_combination_points(room_id: str) -> None:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.delete(RedisKeys.COMBINATION_POINTS.format(room_id))

    ### POINTS ###
    @staticmethod
    def get_points_history(room_id: str) -> list[dict[str, int]]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            points: list[dict[str, int]] = json.loads(lock.client.get(RedisKeys.SCORE_HISTORY.format(room_id), '[]'))

        return points

    @staticmethod
    def set_points_history(room_id: str, points: dict[str, int]) -> None:
        points_history: list[dict[str, int]] = RedisConverter.get_points_history(room_id)
        points_history.append(points)

        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.SCORE_HISTORY.format(room_id), points_history)

    @staticmethod
    def get_deal_players_points(room_id: str) -> dict[str, int]:
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            points: dict[str, int] = json.loads(lock.client.get(RedisKeys.CURRENT_SCORE.format(room_id), '{}'))

        return points

    @staticmethod
    def set_deal_players_points(room_id: str, player_id: str, points: int) -> None:
        db_points: dict[str, int] = RedisConverter.get_deal_combination_points(room_id)

        if player_id in db_points.keys():
            db_points[player_id] += points
        else:
            db_points[player_id] = points

        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.set(RedisKeys.CURRENT_SCORE.format(room_id), json.dumps(db_points))

    @staticmethod
    def remove_deal_players_points(room_id: str):
        with RedisLock(RedisLockKeys.ROOM.format(room_id), Cache()) as lock:
            lock.client.delete(RedisKeys.CURRENT_SCORE.format(room_id))
