import random
from typing import Callable, Optional

from thousand.logic.redis import RedisConverter
from thousand.models.card import Card, Color, Combination, PlayerCard, Sign

GAME_END_POINTS = 1000


class GameLogic:
    @staticmethod
    def shuffle_deck(deck: list[Card]):
        random.shuffle(deck)

    @staticmethod
    def generate_deck() -> list[Card]:
        cards: list[Card] = []

        for color in Color:
            for sign in Sign:
                cards.append(Card(color=color.name, points=sign.value, sign=sign.name))

        GameLogic.shuffle_deck(cards)
        return cards

    @staticmethod
    def check_deal_winner(room_id: str, players_card_casted: list[PlayerCard]) -> str:
        combination: Optional[Color] = RedisConverter.get_combination(room_id)
        cards: list[PlayerCard] = []

        if combination:
            cards = list(filter(lambda x: x.card.color == combination.color, players_card_casted))

        if not cards:
            cards = list(filter(lambda x: x.card.color == players_card_casted[0].card.color, players_card_casted))
            if not cards:
                return players_card_casted[0].player_id

        return max(cards, key=lambda x: x.card.points).player_id

    @staticmethod
    def check_round_winner(room_id: str) -> str:
        points: dict[str, int] = RedisConverter.get_deal_players_points(room_id)
        winner: str = max(points, key=points.get)

        return winner

    @staticmethod
    def check_game_winner(room_id: str) -> Optional[str]:
        pass

    @staticmethod
    def select_next_caster(connected_players: list[str], already_casted_players: list[str]) -> str:
        next_casters: list[str] = [p for p in connected_players if p not in already_casted_players]
        return random.choice(next_casters)

    @staticmethod
    def calculate_single_deal_points(players_cards: list[PlayerCard]):
        pass

    @staticmethod
    def calculate_round_points(players_cards: list[PlayerCard]):
        pass

    @staticmethod
    def check_if_game_ended(room_id: str) -> bool:
        points: list[dict[str, int]] = RedisConverter.get_points_history(room_id)

        if not points:
            return False
        current_max_points: int = max(points[-1].values())
        if current_max_points < GAME_END_POINTS:
            return False
        return True

    @staticmethod
    def check_casted_combination(room_id: str, player_id: str, card: Card, cards_on_hand: list[Card]) -> None:
        combination_filter: Callable = lambda c, name: c.color == card.color and c.sign == name

        if card.sign == Sign.KING.name:
            second_combination_card = len([c for c in cards_on_hand if combination_filter(c, Sign.QUEEN.name)]) > 0
        elif card.sign == Sign.QUEEN.name:
            second_combination_card = len([c for c in cards_on_hand if combination_filter(c, Sign.KING.name)]) > 0
        else:
            return

        if not second_combination_card:
            return

        RedisConverter.add_deal_combination_points(room_id, player_id, Combination[card.color])
