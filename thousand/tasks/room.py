import json

import requests

from thousand.celery import app
from thousand.logic.redis import RedisConverter
from thousand.models.room import PublicRoom, RoomType
from thousand.redis import Cache, RedisKeys, RedisLock, RedisLockKeys


@app.task
def remove_room_on_afk_in_lobby(room_id: str, room_type: RoomType):
    cache: Cache = Cache()
    requests.delete('http://web_server:8000/api/internal/celery', json={'room_id': room_id})

    if room_type == RoomType.PUBLIC:
        RedisConverter.remove_public_room(room_id)
    elif room_type == RoomType.PRIVATE:
        RedisConverter.remove_private_room(room_id)

    db_keys: list[str] = list(
        map(
            lambda x: x.format(room_id), [
                RedisKeys.CONNECTED_PLAYERS,
                RedisKeys.ROOM_HOST,
                RedisKeys.COMBINATION,
                RedisKeys.SCORE_HISTORY,
                RedisKeys.CURRENT_SCORE,
                RedisKeys.STACK,
                RedisKeys.CARDS_ON_HAND,
                RedisKeys.CARDS_ON_TABLE,
            ]
        )
    )
    with RedisLock(RedisLockKeys.ROOM.format(room_id), cache) as lock:
        lock.client.delete_many(db_keys)
