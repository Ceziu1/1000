from collections import Callable
from dataclasses import dataclass
from enum import IntEnum
import json
import random
from string import ascii_uppercase
from uuid import UUID

from marshmallow import Schema, fields, post_load

from thousand.exceptions import PasswordGenerationException
from thousand.redis import Cache, RedisKeys, RedisLock, RedisLockKeys


class RoomType(IntEnum):
    PUBLIC = 1
    PRIVATE = 2


@dataclass
class PublicRoom:
    id: UUID
    max_players: int
    name: str


@dataclass
class PrivateRoom:
    id: UUID
    max_players: int
    password: str

    @staticmethod
    def generate_password() -> str:
        password_generator: Callable[[], str] = lambda: ''.join([random.choice(ascii_uppercase) for _ in range(6)])
        code: str = password_generator()
        with RedisLock(RedisLockKeys.TAKEN_ROOM_CODES, Cache()) as lock:
            taken_codes: str = lock.client.get(RedisKeys.TAKEN_ROOM_CODES, json.dumps([]))
            if not taken_codes:
                return code
            codes_list: list[str] = json.loads(taken_codes)
            for _ in range(6):
                if code in codes_list:
                    code = password_generator()
                else:
                    return code
            raise PasswordGenerationException


class PublicRoomSchema(Schema):
    id = fields.UUID()
    max_players = fields.Integer(validate=lambda x: x in range(2, 5))
    name = fields.String()

    @post_load
    def return_dataclass(self, data, **kwargs):
        return PublicRoom(**data)


class PrivateRoomSchema(Schema):
    id = fields.UUID()
    max_players = fields.Integer(validate=lambda x: x in range(2, 5))
    password = fields.String()

    @post_load
    def return_dataclass(self, data, **kwargs):
        return PrivateRoom(**data)
