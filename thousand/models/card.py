from collections import namedtuple
from dataclasses import asdict, dataclass
from enum import IntEnum

from marshmallow import Schema, fields, post_load
from marshmallow.validate import OneOf

PlayerCard = namedtuple('PlayerCard', ['player_id', 'card'])
PlayerPoints = namedtuple('PlayerPoints', ['player_id', 'points'])


class Combination(IntEnum):
    PIK = 80
    TREFL = 100
    KARO = 40
    KIER = 60


class Color(IntEnum):
    PIK = 1
    TREFL = 2
    KARO = 3
    KIER = 4


class Sign(IntEnum):
    NINE = 0
    JOKER = 2
    QUEEN = 3
    KING = 4
    TEN = 10
    ACE = 11


@dataclass
class Card:
    color: str
    points: int
    sign: str

    def as_dict(self):
        return asdict(self)


class CardSchema(Schema):
    color = fields.String(validate=OneOf(map(lambda x: x.name, Color)))
    points = fields.Integer(validate=OneOf(map(int, Sign)))
    sign = fields.String(validate=OneOf(map(lambda x: x.name, Sign)))

    @post_load
    def return_dataclass(self, data, **kwargs):
        return Card(**data)
