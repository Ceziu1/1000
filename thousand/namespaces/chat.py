from flask import request
from flask_socketio import Namespace, emit, join_room


class ChatNamespace(Namespace):
    def on_join(self, room_id):
        join_room(room_id)

    def on_message(self, data):
        emit('incomming_message', {'message': data['message'], 'player_name': request.sid}, to=data['room_id'])
