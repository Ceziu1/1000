import json
import uuid

from flask import request
from flask_socketio import Namespace, emit, join_room

from thousand.celery import app as celery_app
from thousand.json import ThousandJSONEncoder
from thousand.logic.game import GameLogic
from thousand.logic.redis import RedisConverter
from thousand.models.card import Card, PlayerCard
from thousand.models.room import RoomType
from thousand.tasks.room import remove_room_on_afk_in_lobby

MAX_CARDS_ON_HAND = 6
ROOM_AFK_TIMEOUT = 300


class GameNamespace(Namespace):
    def on_join(self, room_id: str):
        RedisConverter.add_connected_player(room_id, request.sid)
        join_room(room_id)

    def on_create_room(self, data):
        room_id: str = data['room_id']

        RedisConverter.add_room_host(room_id, request.sid)
        RedisConverter.add_connected_player(room_id, request.sid)

        task_id: str = str(uuid.uuid4())
        remove_room_on_afk_in_lobby.apply_async(
            args=(room_id, RoomType(data['room_type'])), task_id=task_id, countdown=ROOM_AFK_TIMEOUT
        )
        join_room(room_id)

        return task_id

    def on_ready(self, data):
        room_id: str = data['room_id']
        task_id: str = data['task_id']

        first_player: str = RedisConverter.get_room_host(room_id)
        connected_players: list[str] = RedisConverter.get_connected_players(room_id)
        if len(connected_players) == 1:
            return False

        deck: list[Card] = GameLogic.generate_deck()
        players_hand_cards: dict[str, list[Card]] = {k: [] for k in connected_players}
        for card_draw in range(MAX_CARDS_ON_HAND):
            for player_id in players_hand_cards.keys():
                players_hand_cards[player_id].append(deck.pop())

        RedisConverter.set_game_state(room_id, 1)
        RedisConverter.set_deck(room_id, deck)

        celery_app.control.revoke(task_id)

        RedisConverter.set_cards_on_hand(room_id, players_hand_cards)

        for player_id, cards in players_hand_cards.items():
            emit('card_draw', [c.as_dict() for c in cards], namespace='/game', to=player_id)

        emit('game_ready', first_player, namespace='/game', room=room_id)

    def on_card_cast(self, data):
        room_id: str = data['room_id']
        player_id: str = request.sid

        card: Card = Card(**data['card'])

        connected_players: list[str] = RedisConverter.get_connected_players(room_id)
        cards_on_hand: dict[str, list[Card]] = RedisConverter.get_cards_on_hand(room_id)
        cards_on_table: list[PlayerCard] = RedisConverter.get_cards_on_table(room_id)
        cards_on_deck: list[Card] = RedisConverter.get_deck(room_id)

        cards_on_hand[player_id].remove(card)
        cards_on_table.append(PlayerCard(player_id, card))

        if len(connected_players) == len(cards_on_table):
            deal_winner: str = GameLogic.check_deal_winner(room_id, cards_on_table)
            points: int = GameLogic.calculate_single_deal_points(cards_on_table)
            RedisConverter.set_deal_players_points(room_id, deal_winner, points)

            if len(cards_on_hand) == 0 and len(cards_on_deck) == 0:
                round_winner: str = GameLogic.check_round_winner(room_id)
                round_points: int = GameLogic.calculate_single_deal_points(cards_on_table)

            if GameLogic.check_if_game_ended(room_id):
                pass
                # end_game_winner: Optional[str] =
                # check end round and game
            # cleanup
            # emit('deal_winner', deal_winner, namespace='/game', room=room_id)
        else:
            GameLogic.check_casted_combination(room_id, player_id, card, cards_on_hand[player_id])

            players_already_casted: list[str] = [p.player_id for p in cards_on_table]
            next_caster: str = GameLogic.select_next_caster(connected_players, players_already_casted)

            RedisConverter.add_card_on_table(room_id, player_id, card)
            emit('next_caster', {'card': card.as_dict(), 'next_caster': next_caster}, namespace='/game', room=room_id)
