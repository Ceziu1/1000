from flask_socketio import SocketIO

from .namespaces.chat import ChatNamespace
from .namespaces.game import GameNamespace


def create_socket():
    sio = SocketIO(logger=True, engineio_logger=False)

    sio.on_namespace(GameNamespace('/game'))
    sio.on_namespace(ChatNamespace('/chat'))

    return sio
