import logging

from flask import Flask

from thousand.api import api
from thousand.exceptions import JSONExceptionHandler
from thousand.json import json
from thousand.sio import create_socket


def create_app():
    sio = create_socket()
    app = Flask(__name__)
    json_error = JSONExceptionHandler()

    sio.init_app(app)
    api.init_app(app)
    json.init_app(app)
    json_error.init_app(app)

    return app


if __name__ == '__main__':
    app = create_app()

    gunicorn_logger = logging.StreamHandler()
    gunicorn_logger.setLevel(logging.DEBUG)
    app.logger.addHandler(gunicorn_logger)

    app.run(debug=True)
