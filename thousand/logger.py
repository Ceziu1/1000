import logging
import sys


class LoggingContext:
    def __init__(self, name: str, level: int, operation: str):
        root = logging.getLogger()
        root.setLevel(level)

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        root.addHandler(handler)

        self.logger = root
        self.level = level
        self.operation = operation

    def __enter__(self):
        self.logger.log(level=self.level, msg=f'Operation {self.operation} performed')

    def __exit__(self, e_type, e_value, stacktrace):
        return True
