from flask_json import as_json
from flask_restful import Resource

from thousand.logic.game import GameLogic
from thousand.models.room import PrivateRoom


class TestAPI(Resource):
    @as_json
    def get(self):
        return {'room_code': PrivateRoom.generate_password(), 'deck': GameLogic.generate_deck()}
