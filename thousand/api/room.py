from typing import Optional
import uuid

from flask import current_app, request
from flask_json import as_json
from flask_restful import Resource, reqparse

from thousand.exceptions import ResourceNotFound, RoomJoinException
from thousand.logic.redis import RedisConverter
from thousand.models.room import PrivateRoom, PrivateRoomSchema, PublicRoom, PublicRoomSchema


class PrivateRoomAPI(Resource):
    @as_json
    def post(self):
        """
        Creates new private room. Returns private room object
        that allows instant connecting to newly created room.
        """

        schema: PrivateRoomSchema = PrivateRoomSchema()

        room: PrivateRoom = schema.load(
            {
                **request.json,
                'id': uuid.uuid4(),
                'password': PrivateRoom.generate_password(),
            }
        )

        RedisConverter.add_taken_code(room.password)
        RedisConverter.add_private_room(room)

        return {'room': schema.dump(room, many=False)}

    @as_json
    def put(self):
        """
        Check if user can join to particular room with given password
        """

        parser = reqparse.RequestParser()
        parser.add_argument('password', type=str, help='Room access code')
        args = parser.parse_args()

        taken_codes: list[str] = RedisConverter.get_taken_codes()
        if args['password'] not in taken_codes:
            raise ResourceNotFound({'room': 'Room with this code does not exist'})

        available_rooms: list[PrivateRoom] = RedisConverter.get_private_rooms()
        private_room_to_join: PrivateRoom = next(filter(lambda x: x.password == args['password'], available_rooms))
        connected_players: list[str] = RedisConverter.get_connected_players(str(private_room_to_join.id))
        if len(connected_players) >= private_room_to_join.max_players:
            raise RoomJoinException({'max_players': 'No more space left to join'})

        return {'id': private_room_to_join.id}, 200


class PublicRoomAPI(Resource):
    @as_json
    def get(self):
        """
        Simple return all available public rooms.
        """
        schema: PublicRoomSchema = PublicRoomSchema()
        available_rooms: list[PublicRoom] = RedisConverter().get_public_rooms()

        return {'rooms': schema.dump(available_rooms, many=True)}

    @as_json
    def post(self):
        """
        Creates new public room. Returns public room object
        that allows instant connecting to newly created room.
        """

        schema: PublicRoomSchema = PublicRoomSchema()
        room: PublicRoom = schema.load({
            **request.json,
            'id': uuid.uuid4(),
        })

        RedisConverter.add_public_room(room)

        return {'room': schema.dump(room, many=False)}

    @as_json
    def put(self):
        """
        Check if user can join to particular room with given id
        """

        parser = reqparse.RequestParser()
        parser.add_argument('id', type=uuid.UUID, help='Room unique id')
        args = parser.parse_args()

        if not RedisConverter.is_game_pending(args['id']):
            raise RoomJoinException({'game_pending': 'Game has already started'})
        available_rooms: list[PublicRoom] = RedisConverter.get_public_rooms()
        public_room_to_join: Optional[PublicRoom] = next(filter(lambda x: x.id == args['id'], available_rooms))
        connected_players: list[str] = RedisConverter.get_connected_players(str(public_room_to_join.id))
        if len(connected_players) >= public_room_to_join.max_players:
            raise RoomJoinException({'max_players': 'No more space left to join'})

        return {'id': public_room_to_join.id}, 200
