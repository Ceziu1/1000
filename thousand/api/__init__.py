from flask_restful import Api

from .internal import InternalCeleryDisconnect
from .room import PrivateRoomAPI, PublicRoomAPI
from .test import TestAPI

api = Api(prefix='/api')

api.add_resource(PublicRoomAPI, '/public_room')
api.add_resource(PrivateRoomAPI, '/private_room')
api.add_resource(InternalCeleryDisconnect, '/internal/celery')
api.add_resource(TestAPI, '/test')
