from flask import request
from flask_restful import Resource
from flask_socketio import close_room, emit


class InternalCeleryDisconnect(Resource):
    def delete(self):
        room_id: str = request.json['room_id']

        emit('afk', namespace='/game', to=room_id)

        close_room(room_id, namespace='/game')
        close_room(room_id, namespace='/chat')
