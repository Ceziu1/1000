import dataclasses
from json import JSONEncoder
from typing import Any
from uuid import UUID

from flask_json import FlaskJSON

from thousand.models.card import Card

json = FlaskJSON()


@json.encoder
def encoder(o):
    if isinstance(o, UUID):
        return str(o)


class ThousandJSONEncoder(JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, Card):
            return dataclasses.asdict(o)
        return super().default(o)
