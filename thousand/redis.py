from datetime import datetime
import logging
import os
from typing import Any, Optional, Union

import redis
from redis import RedisError
from redis.client import Pipeline

ExpireValue = Union[int, datetime, None]


class RedisLockKeys:
    PUBLIC_ROOM = 'room:public:lock'
    PRIVATE_ROOM = 'room:private:lock'
    ROOM = 'room:{}:lock'
    TAKEN_ROOM_CODES = 'rooms:taken_codes:lock'


class RedisKeys:
    PUBLIC_ROOMS_LIST = 'rooms:public'
    PRIVATE_ROOMS_LIST = 'rooms:private'
    TAKEN_ROOM_CODES = 'rooms:taken_codes'
    CONNECTED_PLAYERS = 'room:{}:connected_players'
    ROOM_HOST = 'room:{}:host'
    COMBINATION = 'room:{}:combination'
    COMBINATION_POINTS = 'room:{}:combination_points'
    SCORE_HISTORY = 'room:{}:score_history'
    CURRENT_SCORE = 'room:{}:current_score'
    STACK = 'room:{}:stack'
    CARDS_ON_HAND = 'room:{}:cards_on_hand'
    CARDS_ON_TABLE = 'room:{}:cards_on_table'
    GAME_PENDING = 'room:{}:game_pending'


class Cache:
    def __init__(self):
        self.client = redis.StrictRedis(
            os.environ.get('REDIS_HOST'), os.environ.get('REDIS_PORT'), decode_responses=True
        )
        self.logger = logging.getLogger('Cache')

    @staticmethod
    def _set_with_expire(pipeline: Pipeline, key: str, value: Any, expire: ExpireValue):
        pipeline.set(key, value)
        if isinstance(expire, int):
            pipeline.expire(key, expire)
        elif isinstance(expire, datetime):
            pipeline.expireat(key, expire)
        elif expire is not None:
            raise TypeError('Unexpected expire type')

    def set(self, key: str, value: Any, expire: ExpireValue = None):
        try:
            with self.client.pipeline() as pipeline:
                self._set_with_expire(pipeline, key, value, expire)
                return pipeline.execute()
        except Exception as e:
            self.logger.error(
                'Setting cache %s failed with %s.',
                key,
                e.__class__.__name__,
                extra={
                    'key': key,
                    'value': value,
                    'error': str(e),
                }
            )
            raise RedisError(e)

    def get(self, key: str, default_value: Optional[Any] = None):
        try:
            value: str = self.client.get(key) or default_value  # type: ignore
            return value
        except Exception as e:
            self.logger.error(
                'Getting cache %s failed with %s.', key, e.__class__.__name__, extra={
                    'key': key,
                    'error': str(e),
                }
            )
            return default_value

    def delete(self, key: str):
        try:
            return self.client.delete(key)
        except Exception as e:
            self.logger.error(
                'Getting cache %s failed with %s.', key, e.__class__.__name__, extra={
                    'key': key,
                    'error': str(e),
                }
            )
            return False

    def get_many(self, keys: list[str]) -> dict[str, Any]:
        try:
            values = self.client.mget(keys=keys)
            return dict(zip(keys, values))
        except Exception as e:
            self.logger.error(
                'Getting cache failed with %s.', e.__class__.__name__, extra={
                    'keys': keys,
                    'error': str(e),
                }
            )
            return {}

    def delete_many(self, keys: list[str]) -> bool:
        try:
            return self.client.delete(*keys)
        except Exception as e:
            self.logger.error(
                'Deleting cache failed with %s.', e.__class__.__name__, extra={
                    'keys': keys,
                    'error': str(e),
                }
            )
            return False

    def set_many(self, values: dict[str, Any], expire: ExpireValue = None) -> list:
        try:
            with self.client.pipeline() as pipeline:
                for key, value in values.items():
                    self._set_with_expire(pipeline, key, value, expire)
                return pipeline.execute()
        except Exception as e:
            self.logger.error(
                'Setting cache failed with %s.', e.__class__.__name__, extra={
                    'values': values,
                    'error': str(e),
                }
            )
            raise RedisError(e)

    def keys(self, pattern: Optional[str] = None) -> list[str]:
        if not pattern:
            pattern = '*'
        try:
            return self.client.keys(pattern)
        except Exception as e:
            self.logger.error(
                'Getting keys %s failed with %s.',
                pattern,
                e.__class__.__name__,
                extra={
                    'pattern': pattern,
                    'error': str(e),
                }
            )
            return []


class RedisLock:
    def __init__(
        self,
        name: str,
        connection: Cache,
        timeout: Optional[int] = 20,
        blocking_timeout: Optional[int] = None
    ) -> None:
        self.lock = connection.client.lock(name, timeout=timeout, blocking_timeout=blocking_timeout)
        self.client: Cache = connection

    def __enter__(self):
        self.lock.acquire()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.lock.release()
