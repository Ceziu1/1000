import gameCanvas from "../classes/canvas.js";
import socketManager from "../classes/socket_manager.js";

document.addEventListener("DOMContentLoaded", () => {
  // const canvas_button = document.getElementById("canvas_test_button");
  const create_room_buttons = [
    document.getElementById("private_room_create"),
    document.getElementById("public_room_create"),
  ];
  const join_room_button = document.getElementById("private_room_join");
  const ready_button = document.getElementById("ready");

  const wrapper = document.getElementById("canvas_wrapper");

  gameCanvas.init(wrapper);

  join_room_button.addEventListener("click", () => {
    gameCanvas.init_lobby_wait();
  });
  create_room_buttons.forEach((e) =>
    e.addEventListener("click", () => {
      gameCanvas.init_cover_select();
    })
  );
  ready_button.addEventListener("click", () =>
    socketManager.ready().catch(() => console.log("Nie można być ready teraz"))
  );
});
