import gameCanvas from "../classes/canvas.js";
import socketManager from "../classes/socket_manager.js";
import { roomType } from "../structures.js";

document.addEventListener("DOMContentLoaded", () => {
  const private_room_create = document.getElementById("private_room_create");
  const public_room_create = document.getElementById("public_room_create");
  const join_private_room = document.getElementById("private_room_join");

  const list_rooms_button = document.getElementById("list_rooms_button");
  const list_public_rooms_button = document.getElementById("list_public_rooms");
  const join_private_room_screen_button = document.getElementById(
    "join_private_room_screen"
  );
  const back_to_main_menu_buttons = [
    ...document.getElementsByClassName("back_to_main_menu"),
  ];
  const ready_button = document.getElementById("ready");

  const components = [...document.getElementsByClassName("partial")];
  show_component("main_menu");

  function show_component(component_name) {
    components.forEach((e) => {
      e.style.display = "none";
      if (e.classList.contains(component_name)) e.style.display = "grid";
    });
  }

  function generate_public_room_list(rooms) {
    return rooms.map((e) => {
      let container = document.createElement("div");
      let room_name = document.createElement("p");
      let max_players = document.createElement("p");
      let join = document.createElement("button");

      room_name.innerText = e["name"];
      max_players.innerText = e["max_players"];
      join.innerText = "Join";
      join.addEventListener("click", () => {
        //            join to selected public room
        fetch("/api/public_room", {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ id: e["id"] }),
        })
          .then((response) => response.json())
          .then((data) => socketManager.join(data["id"]))
          .catch((e) => console.log(e))
          .finally(() => {
            show_component("game");
            gameCanvas.init_lobby_wait();
            ready_button.style.display = "none";
          });
        // use can not join to already started game
      });

      container.appendChild(room_name);
      container.appendChild(max_players);
      container.appendChild(join);

      return container;
    });
  }

  private_room_create.addEventListener("click", () => {
    let player_count = Number(
      document.getElementById("private_room_player_count").value
    );

    if (player_count)
      //            join to newly created room
      //            delete room when host leaves
      fetch("/api/private_room", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          max_players: player_count,
        }),
      })
        .then((response) => response.json())
        .then((data) =>
          socketManager.create_room(data["room"]["id"], roomType.PRIVATE)
        )
        .catch((e) => console.log(e))
        .finally(() => show_component("game"));
  });

  public_room_create.addEventListener("click", () => {
    let player_count = Number(
      document.getElementById("public_room_player_count").value
    );
    let room_name = document.getElementById("public_room_name").value;

    if (player_count && room_name)
      //            join to newly created room
      //            delete room when host leaves
      fetch("/api/public_room", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name: room_name,
          max_players: player_count,
        }),
      })
        .then((response) => response.json())
        .then((data) =>
          socketManager.create_room(data["room"]["id"], roomType.PUBLIC)
        )
        .catch((e) => console.log(e))
        .finally(() => show_component("game"));
  });

  join_private_room.addEventListener("click", () => {
    let password = document.getElementById("private_room_password").value;
    //        if status is 200 user can join to lobby
    fetch("/api/private_room", {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ password }),
    })
      .then((response) => response.json())
      .then((data) => socketManager.join(data["id"]))
      .catch((e) => console.log(e))
      .finally(() => {
        show_component("game");
        ready_button.style.display = "none";
      });
    // use can not join to already started game
  });

  // TRANSITIONS
  list_public_rooms_button.addEventListener("click", () => {
    show_component("public_rooms");
    let room_list = document.getElementById("public_rooms_list");
    room_list.innerHTML = "";
    fetch("/api/public_room", {
      method: "GET",
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => {
        const rooms = generate_public_room_list(data["rooms"]);
        rooms.forEach((e) => room_list.appendChild(e));
      })
      .catch((e) => console.log(e));
  });
  list_rooms_button.addEventListener("click", () =>
    show_component("room_type_select")
  );
  join_private_room_screen_button.addEventListener("click", () =>
    show_component("private_rooms")
  );
  back_to_main_menu_buttons.forEach((e) =>
    e.addEventListener("click", () => show_component("main_menu"))
  );
});
