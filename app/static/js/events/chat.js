import socketManager from "../classes/socket_manager.js";

document.addEventListener("DOMContentLoaded", () => {
  const send_message_button = document.getElementById("send_message");

  send_message_button.addEventListener("click", () => {
    const message_field = document.getElementById("message_field");

    if (message_field.value) {
      socketManager.send_message(message_field.value.trim());
      message_field.value = "";
    }
  });
});
