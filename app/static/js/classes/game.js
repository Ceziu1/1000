import Bind from "./bind.js";
import gameCanvas from "./canvas.js";

export default class Game extends Bind {
  constructor(namespace) {
    super(namespace);

    this.events_handlers = {
      afk: this.on_afk,
      game_ready: this.on_game_ready,
      card_draw: this.on_card_draw,
      next_caster: this.on_next_caster,
    };
    this.task_id = "";
    this.current_card_dealer = "";
    this.cards_on_hand = [];
    this.cards_on_board = [];

    this.bind();
  }

  create_room(_id, _type) {
    return new Promise((resolve, reject) => {
      this.io.emit(
        "create_room",
        { room_id: _id, room_type: _type },
        (task_id) => {
          this.task_id = task_id;
          resolve();
        }
      );
    });
  }

  ready(room_id) {
    return new Promise((resolve, reject) =>
      this.io.emit("ready", { task_id: this.task_id, room_id }, (data) => {
        data === false ? reject("Not enough players in this room") : resolve();
      })
    );
  }

  is_card_dealer() {
    return this.io.id === this.current_card_dealer;
  }

  cast_card(room_id, card) {
    this.current_card_dealer = "";
    this.cards_on_hand = this.cards_on_hand.filter((c) => c !== card);

    gameCanvas.clear_canvas();
    gameCanvas.init_hand_cards(this.cards_on_hand);
    gameCanvas.init_cards_on_board(this.cards_on_board);

    this.io.emit("card_cast", { room_id, card });
  }

  //handlers

  on_afk(pointer, data) {
    console.log("you are afk");
  }

  on_game_ready(pointer, data) {
    pointer.current_card_dealer = data;
  }

  on_card_draw(pointer, data) {
    pointer.cards_on_hand = [...pointer.cards_on_hand, ...data];

    gameCanvas.clear_canvas();
    gameCanvas.init_hand_cards(pointer.cards_on_hand);
    gameCanvas.init_cards_on_board(pointer.cards_on_board);
  }

  on_next_caster(pointer, data) {
    pointer.current_card_dealer = data["next_caster"];
    pointer.cards_on_board.push(data["card"]);

    gameCanvas.clear_canvas();
    gameCanvas.init_hand_cards(pointer.cards_on_hand);
    gameCanvas.init_cards_on_board(pointer.cards_on_board);
  }

  on_card_cast(pointer, data) {
    // gameCanvas.draw_deck_cards(data["card"]);
    // pointer.current_card_dealer = data["next_caster"]
    console.log(data);
  }
}
