import Chat from "./chat.js";
import Game from "./game.js";

class SocketManager {
  constructor() {
    this.chat = new Chat("/chat");
    this.game = new Game("/game");

    this.room_id = "";
  }

  join(_id, _type) {
    // this function is for other players
    this.room_id = _id;

    return Promise.all([this.game.join(_id), this.chat.join(_id)]);
  }

  create_room(_id, _type) {
    // this function is called only when new play room is created
    this.room_id = _id;

    return Promise.all([
      this.game.create_room(_id, _type),
      this.chat.join(_id),
    ]);
  }

  ready() {
    return this.game.ready(this.room_id);
  }

  send_message(message) {
    this.chat.message({ message, room_id: this.room_id });
  }

  is_card_dealer() {
    return this.game.is_card_dealer();
  }

  cast_card(card) {
    this.game.cast_card(this.room_id, card);
  }
}

const socketManager = new SocketManager();

export default socketManager;
