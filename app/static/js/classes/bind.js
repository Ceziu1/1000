export default class Bind {
  constructor(namespace) {
    this.io = io(namespace);

    this.events_handlers = {};
  }

  bind() {
    Object.keys(this.events_handlers).forEach((ev) => {
      const fn = this.events_handlers[ev];
      this.io.on(ev, (data) => fn(this, data));
    });
  }

  join(room_id) {
    return new Promise((resolve, reject) => {
      this.io.emit("join", room_id, resolve);
    });
  }
}
