import textureManager from "./texture_manager.js";
import socketManager from "./socket_manager.js";

class GameCanvas {
  constructor() {
    fabric.Object.prototype.setControlsVisibility({
      tl: false,
      mt: false,
      tr: false,
      ml: false,
      mr: false,
      bl: false,
      mb: false,
      br: false,
      mtr: false,
    });
  }

  init(container) {
    // called only once
    this.canvas = new fabric.Canvas("gameboard", {
      backgroundColor: "#74a380",
    });
    this.canvas.setDimensions({
      width: container.clientWidth,
      height: container.clientHeight,
    });

    window.addEventListener("resize", () => {
      this.canvas.setDimensions({
        width: container.clientWidth,
        height: container.clientHeight,
      });
    });
  }

  init_helpers() {
    // for test purposes only
    let arc = new fabric.Circle({
      radius: 150,
      left: this.canvas.width / 2,
      top: this.canvas.height,
      originX: "center",
      originY: "center",
      angle: 180,
      startAngle: 0,
      endAngle: Math.PI,
      stroke: "#000",
      strokeWidth: 5,
      fill: "",
    });

    this.canvas.add(arc);

    for (let i = 1; i < 9; i++) {
      let x = this.canvas.width / 2 + Math.cos(this.deg2rad(180 / 9) * i) * 150;
      let y = this.canvas.height - Math.sin(this.deg2rad(180 / 9) * i) * 150;
      let box = new fabric.Rect({
        originX: "center",
        originY: "center",
        left: x,
        top: y,
        angle: this.rad2deg(Math.cos(this.deg2rad(180 / 9) * i)),
        height: 25,
        width: 25,
        fill: "rgba(255,0,0,0.5)",
      });

      this.canvas.add(box);
    }
  }

  init_lobby_wait() {
    const text = new fabric.Textbox("Wait for the host to begin the game", {
      left: this.canvas.width / 2,
      top: this.canvas.height / 2,
      originX: "center",
      originY: "center",
      textAlign: "center",
      width: this.canvas.width,
      fontSize: 40,
    });
    text.set("fontFamily", "pixelFont");

    this.canvas.add(text);
  }

  init_cover_select() {
    const arrow = (direction) =>
      new fabric.Triangle({
        width: 30,
        height: 30,
        fill: "black",
        originX: "center",
        originY: "center",
        top: this.canvas.height / 2,
        angle: direction === -1 ? -90 : 90,
        lockMovementX: true,
        lockMovementY: true,
      });

    let arrow_left = arrow(-1);
    arrow_left.on("mousedown", this.previous_cover);
    arrow_left.left = this.canvas.width / 2 - 300;
    this.canvas.add(arrow_left);

    let arrow_right = arrow(1);
    arrow_right.on("mousedown", this.next_cover);
    arrow_right.left = this.canvas.width / 2 + 300;
    this.canvas.add(arrow_right);

    let fetch_complete = Promise.all(textureManager.cover_promises);
    fetch_complete.then(() =>
      this.draw_card(
        { left: this.canvas.width / 2, top: this.canvas.height / 2 },
        textureManager.cover_blobs[textureManager.cover_names[0]]
      )
    );
  }

  init_hand_cards(cards) {
    cards.forEach((card, idx, arr) => {
      let x =
        this.canvas.width / 2 +
        Math.cos(this.deg2rad(180 / arr.length) * (arr.length - idx)) * 150;
      let y =
        this.canvas.height -
        50 -
        Math.sin(this.deg2rad(180 / arr.length) * (arr.length - idx)) * 150;
      let angle = this.rad2deg(
        Math.cos(this.deg2rad(180 / arr.length) * (arr.length - idx))
      );
      let [scaleX, scaleY] = [0.4, 0.4];

      this.draw_card(
        { left: x, top: y, angle, scaleX, scaleY },
        textureManager.card_face_blobs[`${card.color}_${card.sign}`],
        {
          mouseup: () => {
            if (socketManager.is_card_dealer()) {
              console.log("Jesteś casterem");
              this.cast_card(card);
            } else {
              console.log("Nie jesteś casterem");
            }
          },
        }
      );
    });
  }

  init_cards_on_board(cards) {
    cards.forEach((card, idx, arr) => {
      let x = 80 * (idx + 1);
      let y = 80;
      let [scaleX, scaleY] = [0.25, 0.25];

      this.draw_card(
        { left: x, top: y, scaleX, scaleY },
        textureManager.card_face_blobs[`${card.color}_${card.sign}`]
      );
    });
  }

  clear_canvas() {
    this.canvas.clear();
    this.canvas.setBackgroundColor("#74a380");
  }

  deg2rad(degrees) {
    return (degrees * Math.PI) / 180;
  }

  rad2deg(radians) {
    return (radians * 180) / Math.PI;
  }

  next_cover() {
    const change_animation_event = {
      onChange: this.canvas.renderAll.bind(this.canvas),
    };
    const cover = this.canvas.getObjects().slice(-1)[0];
    cover.animate(
      { left: "+=300", opacity: 0 },
      {
        ...change_animation_event,
        onComplete: () =>
          cover.animate("left", "-=600", {
            ...change_animation_event,
            duration: 10,
            onComplete: () => {
              textureManager.cover_names.push(
                textureManager.cover_names.shift()
              );
              let blob_obj =
                textureManager.cover_blobs[textureManager.cover_names[0]];
              cover.setSrc(URL.createObjectURL(blob_obj));
              cover.animate(
                { left: "+=300", opacity: 1 },
                change_animation_event
              );
            },
          }),
      }
    );
  }

  previous_cover() {
    const change_animation_event = {
      onChange: this.canvas.renderAll.bind(this.canvas),
    };
    const cover = this.canvas.getObjects().slice(-1)[0];
    cover.animate(
      { left: "-=300", opacity: 0 },
      {
        ...change_animation_event,
        onComplete: () =>
          cover.animate("left", "+=600", {
            ...change_animation_event,
            duration: 10,
            onComplete: () => {
              textureManager.cover_names.unshift(
                textureManager.cover_names.pop()
              );
              let blob_obj =
                textureManager.cover_blobs[textureManager.cover_names[0]];
              cover.setSrc(URL.createObjectURL(blob_obj));
              cover.animate(
                { left: "-=300", opacity: 1 },
                change_animation_event
              );
            },
          }),
      }
    );
  }

  draw_card(props, blob_obj, callbacks = {}) {
    let objectURL = URL.createObjectURL(blob_obj);
    fabric.Image.fromURL(objectURL, (img) => {
      img.set({
        ...props,
        originX: "center",
        originY: "center",
        lockMovementX: true,
        lockMovementY: true,
      });
      Object.entries(callbacks).forEach(([ev, fn]) => img.on(ev, fn));
      this.canvas.add(img);
    });
  }

  cast_card(card) {
    socketManager.cast_card(card);
    console.log(card);
  }
}

const gameCanvas = new GameCanvas();

export default gameCanvas;
