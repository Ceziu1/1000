import Bind from "./bind.js";

export default class Chat extends Bind {
  constructor(namespace) {
    super(namespace);

    this.events_handlers = {
      incomming_message: this.on_message_receive,
    };

    this.bind();
  }

  message(data) {
    this.io.emit("message", data);
  }

  _create_chat_element(message, sender) {
    const main_container = document.createElement("div");
    const user_container = document.createElement("div");
    const time_container = document.createElement("div");
    const message_container = document.createElement("div");

    main_container.appendChild(user_container);
    main_container.appendChild(time_container);
    main_container.appendChild(message_container);

    const current_time = new Date();
    main_container.className = "chat_element";
    user_container.innerText = sender;
    time_container.innerText = current_time.toLocaleTimeString();
    message_container.innerText = message;

    return main_container;
  }

  //handlers

  on_message_receive(pointer, data) {
    const chat_element_container = pointer._create_chat_element(
      data["message"],
      data["player_name"]
    );
    const chat_container = document.getElementById("chat");
    chat_container.appendChild(chat_element_container);
  }
}
