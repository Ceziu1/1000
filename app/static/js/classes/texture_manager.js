import { Color, Sign } from "../structures.js";

class TextureManager {
  constructor() {
    this.cover_blobs = {};
    this.card_face_blobs = {};
    this.cover_promises = [];
    this.cover_names = [];

    fetch("static/cards.json")
      .then((response) => response.json())
      .then((json) =>
        json.forEach((e) => {
          this.cover_promises.push(this.get_cover_blob(e));
          this.cover_names.push(e.split(".")[0]);
        })
      );

    for (const color in Color) {
      for (const sign in Sign) {
        this.get_card_face_blob(`${color}_${sign}`);
      }
    }
  }

  get_card_face_blob(name) {
    return fetch(`static/img/card/${name}.png`)
      .then((response) => response.blob())
      .then((blob) => (this.card_face_blobs[name] = blob));
  }

  get_cover_blob(name) {
    return fetch(`static/img/cover/${name}`)
      .then((response) => response.blob())
      .then((blob) => {
        const key = name.split(".")[0];
        this.cover_blobs[key] = blob;
      });
  }
}

const textureManager = new TextureManager();

export default textureManager;
