export const roomType = {
  PUBLIC: 1,
  PRIVATE: 2,
};

export const Color = {
  PIK: 1,
  TREFL: 2,
  KARO: 3,
  KIER: 4,
};

export const Sign = {
  NINE: 0,
  JOKER: 2,
  QUEEN: 3,
  KING: 4,
  TEN: 10,
  ACE: 11,
};
